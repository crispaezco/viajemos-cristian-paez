<?php

namespace Database\Seeders;

use App\Models\Location;
use Illuminate\Database\Seeder;

class LocationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Location::create([
            'name' => 'Miami',
            'lat' => 25.761636898261038,
            'lng' => -80.19170680921262,
        ]);
        Location::create([
            'name' => 'Orlando',
            'lat' => 28.5393824956663,
            'lng' => -81.37979178870245,
        ]);
        Location::create([
            'name' => 'New York',
            'lat' => 40.698026345494185,
            'lng' => -74.04864378776885,
        ]);
    }
}
