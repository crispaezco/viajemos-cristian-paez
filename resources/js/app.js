require('./bootstrap');

import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'

window.Vue = require('vue');

Vue.component('maps-component', require('./components/MapsComponent.vue').default);

Vue.use(VueGoogleMaps, {
    load: {
        key: process.env.MIX_GOOGLE_API_KEY,
        libraries: 'places'
    },
});
const app = new Vue({
    el: '#app',
});
