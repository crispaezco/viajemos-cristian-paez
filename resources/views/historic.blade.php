@extends('layouts.app')
@section('app')
<h1>Historial de humedad</h1>
<div id="accordion">
    @foreach($locations as $location)
        <div class="card">
            <div class="card-header" id="heading{{$location->id}}">
                <h5 class="mb-0">
                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$location->id}}"
                            aria-expanded="true" aria-controls="collapse{{$location->id}}">
                        {{$location->name}}
                    </button>
                </h5>
            </div>

            <div id="collapse{{$location->id}}" class="collapse {{$loop->first ? 'show' : ''}}"
                 aria-labelledby="heading{{$location->id}}" data-parent="#accordion">
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Fecha y hora</th>
                            <th scope="col">Humedad</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($location->weatherLocations as $item)
                            <tr>
                                <th>{{$item->created_at}}</th>
                                <td scope="row">{{$item->humidity}} %</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endforeach
</div>
@endsection
