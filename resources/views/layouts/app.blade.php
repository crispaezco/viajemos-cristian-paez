<!doctype html>
<html lang="{{app()->getLocale()}}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Prueba Cristian Paez</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
</head>

<body>

<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand" href="/">Weather</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item {{ \Illuminate\Support\Facades\Route::currentRouteName() == 'home' ? 'active' : ''  }}">
                <a class="nav-link"
                   href="/">Home {!! \Illuminate\Support\Facades\Route::currentRouteName() == 'home' ? '<span class="sr-only">(current)</span>' : ''  !!}</a>
            </li>
            <li class="nav-item {{ \Illuminate\Support\Facades\Route::currentRouteName() == 'historic' ? 'active' : ''  }}">
                <a class="nav-link"
                   href="{{route('historic')}}">Historico {!! \Illuminate\Support\Facades\Route::currentRouteName() == 'historic' ? '<span class="sr-only">(current)</span>' : ''  !!}</a>
            </li>
        </ul>
    </div>
</nav>

<main id="app" class="app" role="main">
    <div class="container">
        @yield('app')
    </div>
</main>

<footer class="container">
    <p>&copy; Cristian Paez</p>
</footer>

<script src="{{asset('/js/app.js')}}"></script>
</body>
</html>
