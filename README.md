# viajemos.com
> Prueba técnica - Desarrollador Senior

## Por favor verificar las siguientes dependencias
  * PHP: ^7.4
  * MySQL: ^8.0
  * Composer: ^1.10.0
  * Node.js: ^14.15.1

#### env File
Por favor configure su base de datos local en el siguiente bloque del archivo .env, adicional asegurese de colocar las variables GOOGLE_API_KEY y MIX_GOOGLE_API_KEY la cual se le compartira por correo. Adicional asegurese de configurar las variables para poder consumir el servicio de Yahoo que tambien seran compartidos por correo 
```
...
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=database_name
DB_USERNAME=root
DB_PASSWORD=
...
GOOGLE_API_KEY=
MIX_GOOGLE_API_KEY="${GOOGLE_API_KEY}"
...
YAHOO_WEATHER_APP_ID=TEST
YAHOO_WEATHER_CLIENT_ID=
YAHOO_WEATHER_CLIENT_SECRET=
```

#### Migración y seeders
Una vez configure las variables de entorno proceda a ejecutar migraciones y seeders.
* ##### Migración && Seed
```sh
$ php artisan migrate
$ php artisan db:seed
```
### Para ejecutar el proyecto corra el siguiente comando
Por defecto se iniciará en la ruta [127.0.0.1:8000](http://127.0.0.1:8000) y en caso de tener el puerto ocupado por favor revise la url en consola
```sh
$ php artisan serve
```

## Navegación por url's del sitio
* __/__ [localhost:8000](http://localhost:8000)
* __/historic__ [localhost:8000/historic](http://localhost:8000/historic)
