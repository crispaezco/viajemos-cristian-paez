<?php

namespace App\Http\Controllers;

use App\Models\Location;

class PublicController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function weathersLocations()
    {
        return Location::get()->each->append('weather');
    }

    public function historic()
    {
        $locations = Location::query()->with('weatherLocations')->get();
        return view('historic', compact('locations'));
    }
}
