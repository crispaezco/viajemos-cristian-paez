<?php

namespace App\Providers;

use App\Util\YahooWeatherApi;
use Illuminate\Support\ServiceProvider;

class YahooWeatherApiProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(YahooWeatherApi::class, function ($app) {
            $config = config("services.yahoo_weather");
            return new YahooWeatherApi(
                $config["app_id"],
                $config["client_id"],
                $config["client_secret"]
            );
        });
    }
}
