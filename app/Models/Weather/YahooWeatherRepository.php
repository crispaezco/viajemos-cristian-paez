<?php

namespace App\Models\Weather;

use App\Util\YahooWeatherApi;

class YahooWeatherRepository
{
    protected $api = null;

    public function __construct(YahooWeatherApi $api)
    {
        $this->api = $api;
    }

    public function find($data)
    {
        return $this->api->makeRequest($data);
    }
}
