<?php

namespace App\Models;

use App\Models\Weather\YahooWeatherRepository;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Location extends Model
{
    use HasFactory;

    protected $weather = null;

    protected $casts = ['lat'=>'float', 'lng'=>'float', ];

    public function __construct()
    {
        $this->weather = App::make(YahooWeatherRepository::class);
    }

    public function weatherLocations()
    {
        return $this->hasMany(WeatherLocation::class);
    }

    public function getWeatherAttribute()
    {
        $data = $this->weather->find(["lat" => $this->lat, "lon" => $this->lng]);
        $this->weatherLocations()->create(['humidity' => optional($data->current_observation->atmosphere)->humidity]);
        return $data;
    }
}
