<?php


namespace App\Util;


use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class YahooWeatherApi
{
    private $appId;
    private $clientId;
    private $clientSecret;
    private $endpoint;

    public function __construct($appId, $clientId, $clientSecret)
    {
        $this->appId = $appId;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->endpoint = "https://weather-ydn-yql.media.yahoo.com/forecastrss";
    }

    public function buildBaseString($baseURI, $method, $params)
    {
        $r = array();
        ksort($params);
        foreach ($params as $key => $value) {
            $r[] = "$key=" . rawurlencode($value);
        }
        return $method . "&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
    }

    public function buildAuthorizationHeader($oauth)
    {
        $r = 'Authorization: OAuth ';
        $values = array();
        foreach ($oauth as $key => $value) {
            $values[] = "$key=\"" . rawurlencode($value) . "\"";
        }
        $r .= implode(', ', $values);
        return $r;
    }


    public function makeRequest($query = [])
    {

        $url = $this->endpoint;
        $app_id = $this->appId;
        $consumer_key = $this->clientId;
        $consumer_secret = $this->clientSecret;

        $query['format'] = 'json';
        $query['u'] = 'c';

        $oauth = array(
            'oauth_consumer_key' => $consumer_key,
            'oauth_nonce' => uniqid(mt_rand(1, 1000)),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0'
        );

        $base_info = $this->buildBaseString($url, 'GET', array_merge($query, $oauth));
        $composite_key = rawurlencode($consumer_secret) . '&';
        $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
        $oauth['oauth_signature'] = $oauth_signature;

        $header = array(
            $this->buildAuthorizationHeader($oauth),
            'X-Yahoo-App-Id: ' . $app_id
        );
        $options = array(
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_HEADER => false,
            CURLOPT_URL => $url . '?' . http_build_query($query),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);

        $return_data = json_decode($response);
        return $return_data;
    }

}
